from flask import render_template
from app import app

@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Lupa'}
    posts = [
        {
            'author': {'username': 'Jão'},
            'body': 'Im gonna try speak in english'
        },
        {
            'author': {'username': 'Euzin'},
            'body': 'The Dr Strange movie will be so cool!'
        }
    ]
    return render_template('index.html', title='Inicio', user=user, posts=posts)